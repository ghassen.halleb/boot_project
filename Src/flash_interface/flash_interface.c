#include "flash_interface.h"

HAL_StatusTypeDef flash_write_page(uint8_t *buffer, uint32_t start_address, uint16_t size)
{
    HAL_StatusTypeDef status;
    FLASH_EraseInitTypeDef EraseInitStruct;
    uint32_t SECTORError = 0;
    uint16_t i;
    uint32_t word_data;
    /*Check that address is in flash */
    if (isInFlash(start_address) == HAL_ERROR)
    {
        return HAL_ERROR;
    }
    if (isInFlash(start_address + size) == HAL_ERROR)
    {
        return HAL_ERROR;
    }
    /*Before writing page must be erased */

    /*Cleat flash flags from previous operations */
    __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_WRPERR | FLASH_FLAG_PGERR | FLASH_FLAG_OPTVERR);

    /* Set erase type as page erase */
    EraseInitStruct.TypeErase = FLASH_TYPEERASE_PAGES;

    /* Set the start address of the erase operation */
    EraseInitStruct.PageAddress = start_address;

    /*Erase one single page at a time */
    EraseInitStruct.NbPages = 1;

    /* Unlock the flash to be able to erase */
    HAL_FLASH_Unlock();

    /* Perform the erase */
    status = HAL_FLASHEx_Erase(&EraseInitStruct, &SECTORError);

    /*Lock the flash again */
    HAL_FLASH_Lock();

    /*If the erase went well, perform the writing*/
    /* The writing is done by 32 bit. buffer size has to be multiple of 4. if not add some paddings */
    if (status == HAL_OK)
    {
        /*Test if size is a multiple of 4 */
        if (size % 4 != 0)
        {
            /*Add zeros in the end to have a multiple 4 size*/
            uint8_t padding = 4 - (size % 4);
            memset(buffer + size, 0, padding);
        }
        /*write 4 bytes of data in each loop */
        for (i = 0; i < size; i += 4)
        {
            word_data = buffer[i] + (buffer[i + 1] << 8) + (buffer[i + 2] << 16) + (buffer[i + 3] << 24);
            HAL_FLASH_Unlock();
            status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, start_address + i, word_data);
            HAL_FLASH_Lock();
            if (status != HAL_OK)
            {
                return status;
            }
        }
    }
    else
    {
        HAL_FLASH_Lock();
        return status;
    }

    return status;
}

HAL_StatusTypeDef write_application_tampon(application_tampon_t application_tampon, uint8_t checksum)
{
    /*Copy the whole page in ram */
    uint8_t temp_buffer[1024];
    /*Point to the start of tampons's page */
    char *pagePointer = (char *)TAMPON_PAGE_ADDR;
    /* Copy content of the page*/
    memcpy(temp_buffer, pagePointer, 1024);
    /*Edit the application tampon */
    temp_buffer[APP_TAMPON_PAGE_OFFSET] = application_tampon;
    temp_buffer[APP_CHECKSUM_PAGE_OFFSET] = checksum;
    flash_write_page(temp_buffer, TAMPON_PAGE_ADDR, 1024);
}

HAL_StatusTypeDef isInFlash(uint32_t address)
{
    /*Test if the address given is in the range of the flashable application */
    if (address >= APP_START_ADDR && address < FLASH_START_ADDR + FLASH_SIZE)
    {
        return HAL_OK;
    }
    else
    {
        return HAL_ERROR;
    }
}