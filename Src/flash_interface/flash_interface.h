#ifndef FLASH_INTERFACE_H
#define FLASH_INTERFACE_H
#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_flash.h"

#define FLASH_START_ADDR 0x8000000
#define APP_START_ADDR FLASH_START_ADDR + 0x4000
#define TAMPON_ADDR 0x801d000
#define CHECKSUM_ADDR 0x801d001
#define TAMPON_PAGE_ADDR   0x801d000
#define APP_TAMPON_PAGE_OFFSET TAMPON_ADDR - TAMPON_PAGE_ADDR 
#define APP_CHECKSUM_PAGE_OFFSET CHECKSUM_ADDR - TAMPON_PAGE_ADDR 
#define FLASH_SIZE  128 * 1024

typedef enum{
    APPLICATION_INVALID_TAMPON = 0x00,
    APPLICATION_VALID_TAMPON = 0x01
} application_tampon_t;

HAL_StatusTypeDef flash_write_page(uint8_t *buffer, uint32_t start_address, uint16_t size);
HAL_StatusTypeDef isInFlash(uint32_t address);
HAL_StatusTypeDef write_application_tampon(application_tampon_t application_tampon, uint8_t checksum);


#endif