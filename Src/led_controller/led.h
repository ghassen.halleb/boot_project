/*
 * led.h
 *
 *  Created on: Sep 30, 2019
 *      Author: safa
 */

#ifndef SRC_LED_LED_H_
#define SRC_LED_LED_H_

#include "stm32f1xx_hal.h"

void init_leds();
void deinit_leds();

#endif /* SRC_LED_LED_H_ */
