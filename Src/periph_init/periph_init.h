#ifndef PERIPH_INIT_H_
#define PERIPH_INIT_H_
#include "stm32f1xx_hal.h"
#include "led.h"
#include "transport_protocol.h"

init_status_t init_peripheral();
init_status_t deinit_peripheral();
void SystemClock_Config(void);
void MX_WWDG_Init(void);
void Error_Handler(void);

WWDG_HandleTypeDef hwwdg;

#endif