C_SOURCES +=  \
Src/main.c \
Src/stm32f1xx_it.c \
Src/stm32f1xx_hal_msp.c \
Src/system_stm32f1xx.c \
Src/led_controller/led.c \
Src/frame_wraper_module/frame_wraper.c \
Src/flash_interface/flash_interface.c \
Src/transport_protocol/transport_protocol.c \
Src/reprog_layer/reprog_command.c \
Src/periph_init/periph_init.c \

C_INCLUDES +=-ISrc/led_controller \
-ISrc/frame_wraper_module \
-ISrc/flash_interface \
-ISrc/transport_protocol \
-ISrc/reprog_layer \
-ISrc/periph_init \
