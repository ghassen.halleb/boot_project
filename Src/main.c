/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  init_peripheral();

  /*Local variables */
  frame_t input_frame;
  frame_t output_frame;
  uint8_t response_data[ACK_NACK_SIZE];
  uint8_t response_size;

  /* Initialize the current size of data in ram */
  currentDataInRAmBuffer = 0;

  /* Check if the application in flash is valid. if yes jump to it*/
  if (application_is_valid() == APPLICATION_VALID_TAMPON)
  {
    jump_to_application();
  }
  /*Application is not valid stay in boot mode */
  while (1)
  {
    /* Initialize the transport buffer */
    memset(TP_buffer, 0, sizeof(TP_buffer));
    /* if a command is received */
    if (TP_receive_frame(TP_buffer) == HAL_OK)
    {
      /* Deserialize the buffer in a struct */
      deserialize_frame(TP_buffer, &input_frame);
      /* Check if command id is valid */
      if ((commandType_t)input_frame.command < (commandType_t)UNKNOWN_CMD)
      {
        /* Execute the command */
        reprog_command_execute((commandType_t)input_frame.command,
                               input_frame.data,
                               input_frame.frameSize,
                               response_data,
                               &response_size);
        /*Prepare the reponse */
        frame_build_ack_response(response_data,
                                 response_size,
                                 &input_frame,
                                 &output_frame);
        /* Serialize the response from a buffer to a struct */
        serialize_message(&output_frame, TP_buffer);
        /*Send the response on the transport protocol */
        TP_send(TP_buffer, ACK_NACK_SIZE - CHECKSUM_SIZE);
      }
    }
    }
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
