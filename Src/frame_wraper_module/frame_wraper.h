#ifndef FRAME_WRAPER_H
#define FRAME_WRAPER_H
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define FRAME_MAX_SIZE 128u
#define FRAME_DATA_OFFSET 3u
#define FRAME_LEN_OFFSET 0u
#define FRAME_ID_OFFSET 2u
#define FRAME_COMMAND_OFFSET 1u

#define CHECKSUM_SIZE 1
#define HEADER_SIZE 3

#define ACK_NACK_SIZE 10

typedef struct
{
    uint8_t frameSize;
    uint8_t command;
    uint8_t frameId;
    uint8_t data[FRAME_MAX_SIZE];
    uint8_t checksum;
    union {
        uint8_t data[FRAME_MAX_SIZE + 4];
    } rawData;
} frame_t;

typedef enum
{
    CRC_OK,
    CRC_WRONG
} crc_check_t;

typedef enum
{
    FRAME_VALID = 0x01u,
    FRAME_INVALID = 0x02u
} frame_valid_t;

void serialize_message(frame_t *input_frame, uint8_t *output_buffer);
void frame_build_ack_response(uint8_t *response_buffer, uint8_t response_size, frame_t *in_frame, frame_t *out_frame);
void deserialize_frame(uint8_t *buffer, frame_t *frame);
#endif //FRAME_WRAPER_H