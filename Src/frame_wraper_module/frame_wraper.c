#include "frame_wraper.h"
#include "reprog_command.h"

/*Static frmeId increments each time a frame is sent */
static s_frameId = 0;

/* private fucntions header*/
static void deserialize_header(uint8_t *buffer, frame_t *frame);
static void deserialize_data(uint8_t *buffer, frame_t *frame);

/*Copy the header of a frame from a buffer to a struct */
static void deserialize_header(uint8_t *buffer, frame_t *frame)
{
    memcpy((uint8_t *)&frame->frameSize, buffer, HEADER_SIZE);
}

/*Copy the data of a frame from a buffer to a struct */
static void deserialize_data(uint8_t *buffer, frame_t *frame)
{
    uint8_t length = frame->frameSize;
    memcpy(frame->data, buffer + HEADER_SIZE, length);
    frame->checksum = buffer[length + HEADER_SIZE];
}

/*Copy all the frame from a buffer to a struct */
void deserialize_frame(uint8_t *buffer, frame_t *frame)
{
    deserialize_header(buffer, frame);
    deserialize_data(buffer, frame);
}

/* Build the response frame based on the response buffer */
void frame_build_ack_response(uint8_t *response_buffer, uint8_t response_size, frame_t *in_frame, frame_t *out_frame)
{
    /*First byte of response contains the source frameId */
    out_frame->data[0] = in_frame->frameId;
    /*Add the response data in the struct */
    memcpy(&out_frame->data[1], &response_buffer[0], response_size);

    /*Build output header*/
    out_frame->command = ACK_CMD;
    out_frame->frameSize = response_size + 1;
    out_frame->frameId = s_frameId;

    /*Increment the frameId for the next command */
    s_frameId++;
}

void serialize_message(frame_t *input_frame, uint8_t *output_buffer)
{
    /*Copy the message from a frame to a buffer */
    memcpy(output_buffer, (uint8_t *)input_frame, input_frame->frameSize + HEADER_SIZE);
}