#ifndef REPROG_H
#define REPROG_H
#include "stdint.h"
#include "transport_protocol.h"

#define REPROG_BUFFER_SIZE 1024u
#define REPROG_RESPONSE_SIZE 4u

uint8_t reprogRamBuffer[REPROG_BUFFER_SIZE];
uint8_t reporgResponse[REPROG_RESPONSE_SIZE];
uint32_t currentDataInRAmBuffer;

#endif