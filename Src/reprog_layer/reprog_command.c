#include "reprog_command.h"
#include "flash_interface.h"
#include "reprog.h"

typedef void (*pFunction)(void);

application_tampon_t application_is_valid()
{
    /*Read the application info in the flash memory */
    uint8_t tampon = *(uint8_t *)TAMPON_PAGE_ADDR;
    return tampon;
}


void jump_to_application()
{
    /*Function pointer type */
    pFunction JumpToApplication;
    /*Jump address */
    uint32_t JumpAddress;

    /*De-init all the peripherals */
    deinit_peripheral();

    /*Disable the interrupts */
    __disable_irq();

    /*Point the vector table to the application vector table */
    SCB->VTOR = APP_START_ADDR;

    /*Sets the main stack pointer to application stack pointer */
    __set_MSP(*(__IO uint32_t *)(APP_START_ADDR));

    /*Application startup is located at Start address + 4 */
    JumpAddress = *(__IO uint32_t *)(APP_START_ADDR + 4);
    JumpToApplication = (pFunction)JumpAddress;
    /* Jump to startup */
    JumpToApplication();
}

/*Commands Callbacks */
void callback_NO_CMD(uint8_t *in_buffer, uint8_t in_size, uint8_t *out_buffer, uint8_t *out_size)
{
}
void callback_ACK_CMD(uint8_t *in_buffer, uint8_t in_size, uint8_t *out_buffer, uint8_t *out_size)
{
}
void callback_W2RAM_CMD(uint8_t *in_buffer, uint8_t in_size, uint8_t *out_buffer, uint8_t *out_size)
{
    /*Copy the in_buffer in reprogRamBuffer in Ram */
    /*Check if size of data does not exceed */
    if (currentDataInRAmBuffer + in_size <= REPROG_BUFFER_SIZE)
    {
        /*Copy data to ram buffer and update the current size */
        memcpy(reprogRamBuffer + currentDataInRAmBuffer, in_buffer, in_size);
        currentDataInRAmBuffer += in_size;

        /*Copy current size to the response message */
        memcpy(&out_buffer[0], (uint8_t *)&currentDataInRAmBuffer, sizeof(currentDataInRAmBuffer));
        *out_size = sizeof(currentDataInRAmBuffer);
    }
    else
    {
        /*If an error occurs initialize the data size */
        currentDataInRAmBuffer = 0;
        reprog_command_status = COMMAND_WRONG;

        /*Copy current size to the response message */
        memcpy(&out_buffer[0], (uint8_t *)&currentDataInRAmBuffer, sizeof(currentDataInRAmBuffer));
        *out_size = sizeof(currentDataInRAmBuffer);
    }
}
void callback_W2FLASH_CMD(uint8_t *in_buffer, uint8_t in_size, uint8_t *out_buffer, uint8_t *out_size)
{
    uint32_t write_address;
    /*Get the write address from the command */
    memcpy((uint8_t *)&write_address, &in_buffer[0], sizeof(write_address));
    /*Write the buffer in the ram from the start address */
    if (flash_write_page(reprogRamBuffer, write_address, currentDataInRAmBuffer) == HAL_OK)
    {
        /*Prepare the response */
        memcpy(&reporgResponse[0], (uint8_t *)&write_address, sizeof(write_address));
        *out_size = sizeof(write_address);
    }
    else
    {
        write_address = 0;
        /*Prepare the response */
        memcpy(&reporgResponse[0], (uint8_t *)&write_address, sizeof(write_address));
        *out_size = sizeof(write_address);
    }
    
    currentDataInRAmBuffer = 0;
    memset(reprogRamBuffer, 0, sizeof(reprogRamBuffer));
}

void callback_FLUSHRAM_CMD(uint8_t *in_buffer, uint8_t in_size, uint8_t *out_buffer, uint8_t *out_size)
{
    /*Initialize the ram buffer */
    currentDataInRAmBuffer = 0;
    memset(reprogRamBuffer, 0, sizeof(reprogRamBuffer));
}

void callback_JUMP2APP_CMD(uint8_t *in_buffer, uint8_t in_size, uint8_t *out_buffer, uint8_t *out_size)
{
    /*Perform a jump to application */
    jump_to_application();
}

void callback_CHECK_APP_CMD(uint8_t *in_buffer, uint8_t in_size, uint8_t *out_buffer, uint8_t *out_size)
{
    uint32_t start_address;
    uint32_t i, size;
    uint8_t checksum = 0;

    /*Get the start address of the application*/
    memcpy((uint8_t *)&start_address, &in_buffer[0], sizeof(start_address));

    /*Get the size of the application */
    memcpy((uint8_t *)&size, &in_buffer[sizeof(start_address)], sizeof(size));

    
    char *p_address_content = (char *)start_address;
    /*Compute the checksum of the application */
    for (i = 0; i < size; i++)
    {
        checksum ^= *(p_address_content + i);
    }
    if (checksum == in_buffer[sizeof(start_address) + sizeof(size)])
    {
        write_application_tampon(APPLICATION_VALID_TAMPON, checksum);
    }
    else
    {
        write_application_tampon(APPLICATION_INVALID_TAMPON, 0xff);
    }

    memcpy(out_buffer, &checksum, sizeof(checksum));
}
void reprog_command_execute(uint8_t command, uint8_t *in_buffer, uint8_t in_size, uint8_t *out_buffer, uint8_t *out_size)
{
    switch (command)
    {
    case NO_CMD:
        callback_NO_CMD(in_buffer, in_size, out_buffer, out_size);
        break;
    case ACK_CMD:
        callback_ACK_CMD(in_buffer, in_size, out_buffer, out_size);
        break;
    case W2RAM_CMD:
        callback_W2RAM_CMD(in_buffer, in_size, out_buffer, out_size);
        break;
    case W2FLASH_CMD:
        callback_W2FLASH_CMD(in_buffer, in_size, out_buffer, out_size);
        break;
    case FLUSHRAM_CMD:
        callback_FLUSHRAM_CMD(in_buffer, in_size, out_buffer, out_size);
        break;
    case JUMP2APP_CMD:
        callback_JUMP2APP_CMD(in_buffer, in_size, out_buffer, out_size);
        break;
    case CHECK_APP_CMD:
        callback_CHECK_APP_CMD(in_buffer, in_size, out_buffer, out_size);
    default:
        break;
    }
}

