#ifndef REPROG_COMMAND_H
#define REPROG_COMMAND_H
#include "reprog.h"
#include "reprog_command.h"
#include "flash_interface.h"
#include "periph_init.h"

typedef enum
{
    COMMAND_OK = 0x01,
    COMMAND_WRONG = 0x02
} command_status_t;

typedef enum
{
    NO_CMD = 0x00u,
    ACK_CMD = 0x01u,
    W2RAM_CMD = 0x02u,
    W2FLASH_CMD = 0x03u,
    FLUSHRAM_CMD = 0x04u,
    JUMP2APP_CMD = 0x05u,
    CHECK_APP_CMD = 0x06u,
    UNKNOWN_CMD = 0x07u
} commandType_t;

command_status_t reprog_command_status;

void callback_NO_CMD(uint8_t *in_buffer, uint8_t in_size, uint8_t *out_buffer, uint8_t *out_size);
void callback_ACK_CMD(uint8_t *in_buffer, uint8_t in_size, uint8_t *out_buffer, uint8_t *out_size);
void callback_W2RAM_CMD(uint8_t *in_buffer, uint8_t in_size, uint8_t *out_buffer, uint8_t *out_size);
void callback_W2FLASH_CMD(uint8_t *in_buffer, uint8_t in_size, uint8_t *out_buffer, uint8_t *out_size);
void callback_FLUSHRAM_CMD(uint8_t *in_buffer, uint8_t in_size, uint8_t *out_buffer, uint8_t *out_size);
void callback_CHECK_APP_CMD(uint8_t *in_buffer, uint8_t in_size, uint8_t *out_buffer, uint8_t *out_size);
void callback_JUMP2APP_CMD(uint8_t *in_buffer, uint8_t in_size, uint8_t *out_buffer, uint8_t *out_size);
void reprog_command_execute(uint8_t command, uint8_t *in_buffer, uint8_t in_size, uint8_t *out_buffer, uint8_t *out_size);
application_tampon_t application_is_valid();
#endif //REPROG_COMMAND_H