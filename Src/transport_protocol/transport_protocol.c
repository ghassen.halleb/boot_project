#include "transport_protocol.h"

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static uint8_t TP_compute_crc(uint8_t *buffer, uint8_t size);
static frame_valid_t TP_check_checksum(uint8_t *buffer, uint8_t size);

static init_status_t MX_USART1_UART_Init(void)
{
  init_status_t status = INIT_OK;

 
  TP_HANDLER.Instance = USART1;
  TP_HANDLER.Init.BaudRate = 115200;
  TP_HANDLER.Init.WordLength = UART_WORDLENGTH_8B;
  TP_HANDLER.Init.StopBits = UART_STOPBITS_1;
  TP_HANDLER.Init.Parity = UART_PARITY_NONE;
  TP_HANDLER.Init.Mode = UART_MODE_TX_RX;
  TP_HANDLER.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  TP_HANDLER.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&TP_HANDLER) != HAL_OK)
  {
    status = INIT_ERROR;
  }
  return status;
}

static init_status_t MX_USART1_UART_Deinit(void)
{
  return HAL_UART_DeInit(&TP_HANDLER);
}

/**
  * @brief abstract call of init 
  * @param None
  * @retval None
  */
init_status_t TP_init(transport_protocol_t tp)
{
  init_status_t status = INIT_OK;
  if (tp == NO_TP)
  {
    status = INIT_NO_INIT;
  }
  else if (tp == UART_TP)
  {
    status = MX_USART1_UART_Init();
  }
  else
  {
    status = INIT_ERROR;
  }
  return status;
}

init_status_t TP_deinit(transport_protocol_t tp)
{
  init_status_t status = INIT_OK;
  if (tp == NO_TP)
  {
    status = INIT_NO_INIT;
  }
  else if (tp == UART_TP)
  {
    status = MX_USART1_UART_Deinit();
  }
  else
  {
    status = INIT_ERROR;
  }
}

TP_STATUS_TYPE TP_receive_data(uint8_t *buffer, uint8_t size)
{
  return HAL_UART_Receive(&TP_HANDLER, buffer + TP_HEADER_SIZE, size, 5000);
}

TP_STATUS_TYPE TP_receive_header(uint8_t *buffer)
{
  return HAL_UART_Receive(&TP_HANDLER, buffer, TP_HEADER_SIZE, 5000);
}

TP_STATUS_TYPE TP_send(uint8_t *buffer, uint8_t size)
{
  uint8_t checksum = TP_compute_crc(buffer, size);
  if (HAL_UART_Transmit(&TP_HANDLER, buffer, size, 5000) != HAL_OK)
  {
    return HAL_ERROR;
  }
  if (HAL_UART_Transmit(&TP_HANDLER, &checksum, sizeof(checksum), 5000) != HAL_OK)
  {
    return HAL_ERROR;
  }
}

TP_STATUS_TYPE TP_receive_frame(uint8_t *buffer)
{
  if (TP_receive_header(buffer) != HAL_OK)
  {
    return HAL_ERROR;
  }

  uint8_t length = TP_buffer[FRAME_LEN_OFFSET];

  if (TP_receive_data(buffer, length + CHECKSUM_SIZE) != HAL_OK)
  {
    return HAL_ERROR;
  }

  if (TP_check_checksum(buffer, length) != FRAME_VALID)
  {
    return HAL_ERROR;
  }

  return HAL_OK;
}

static uint8_t TP_compute_crc(uint8_t *buffer, uint8_t size)
{
  uint8_t checksum = 0;
  uint8_t i;
  for (i = 0; i < size + TP_HEADER_SIZE; i++)
  {
    checksum ^= buffer[i];
  }
  return checksum;
}

static frame_valid_t TP_check_checksum(uint8_t *buffer, uint8_t size)
{
  uint8_t checksum = TP_compute_crc(buffer, size);

  if (checksum == buffer[size + TP_HEADER_SIZE])
  {
    return FRAME_VALID;
  }
  else
  {
    return FRAME_INVALID;
  }
}