#ifndef TRANSPORT_PROTOCOL_H
#define TRANSPORT_PROTOCOL_H
#include "stm32f1xx_hal.h"
#include "frame_wraper.h"

#define TP_HEADER_SIZE 3
#define PROJECT_TP UART_TP

uint8_t TP_buffer[132];

#if (PROJECT_TP == UART_TP)
#define TP_STATUS_TYPE HAL_StatusTypeDef
#endif

typedef enum
{
    INIT_OK,
    INIT_ERROR,
    INIT_NO_INIT
} init_status_t;

typedef enum
{
    NO_TP,
    UART_TP,
    SPI_TP, //Not yet implemented

} transport_protocol_t;

init_status_t TP_init(transport_protocol_t tp);
init_status_t TP_deinit(transport_protocol_t tp);
TP_STATUS_TYPE TP_receive_header(uint8_t *buffer);
TP_STATUS_TYPE TP_receive_data(uint8_t *buffer, uint8_t size);
TP_STATUS_TYPE TP_send(uint8_t *buffer, uint8_t size);
TP_STATUS_TYPE TP_receive_frame(uint8_t *buffer);

#if (PROJECT_TP == UART_TP)
UART_HandleTypeDef huart1;
#define TP_HANDLER huart1
#else
#define TP_HANDLER NULL
#endif //PROJECT_TP

#endif //TRANSPORT_PROTOCOL_H